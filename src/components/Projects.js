import React, {Component} from 'react';
import ProjectsItem from './ProjectsItem';

class Projects extends Component {
    deleteProject(id){
        this.props.onDelete(id);
    }
    renderItems() {
        let projectItems;
        if (this.props.projects) {
            projectItems = this.props.projects.map(project => {
                return (<ProjectsItem key={project.title} project={project} onDelete={this.deleteProject.bind(this)}/>);
            });

            return projectItems;
        }

        else {
            return (

                <h3>No projects available</h3>
            )
                ;
        }
    }
    render() {
        return (
            <div>
                Projects List :
                <ul>
                    {this.renderItems()}
                </ul>
            </div>
        );

    }
}
export default Projects;